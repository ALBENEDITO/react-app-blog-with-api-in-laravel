import React from "react";
import { Link } from "react-router-dom";

function Header({ visible }) {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div className="container">
        <Link className="navbar-brand" to="/">
          Start Bootstrap
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarResponsive"
          aria-controls="navbarResponsive"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarResponsive">
          <ul className="navbar-nav ml-auto">
            {!visible ? (
              <li className="nav-item">
                <Link className="nav-link" to="/login">
                  Login
                </Link>
              </li>
            ) : (
              <>
                <li className="nav-item">
                  <Link className="nav-link" to="/categories">
                    Categorias
                  </Link>
                </li>
                <li className="nav-item">
                  <button
                    className="btn btn-default"
                    onClick={(e) => {
                      e.preventDefault();
                      sessionStorage.removeItem("meutoken");
                      window.location.href = "/";
                    }}
                    style={{ color: "rgba(255,255,255,.5)" }}
                  >
                    Logout
                  </button>
                </li>
              </>
            )}
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Header;
