import React from "react";
import { Link } from "react-router-dom";

export default class Item extends React.Component {
  render() {
    const { post, read } = this.props;
    const pathImage =
      post.image === null ? "http://placehold.it/750x300" : post.image_url;
    return (
      <div className="card mb-4">
        <img
          className="card-img-top"
          src={`${pathImage}`}
          alt="Card image cap"
          title={"Card image cap"}
        />
        <div className="card-body">
          <h2 className="card-title">{post.title}</h2>
          <p className="card-text">{post.body}</p>
          {!read && (
            <Link to={`/posts/show/${post.id}`} className="btn btn-primary">
              Read More &rarr;
            </Link>
          )}
        </div>
        <div className="card-footer text-muted">
          Category{" "}
          <Link to={`/posts/category/${post.category_id}`}>
            {post.category.name}
          </Link>{" "}
          <br />
          Posted on January {post.created_at.toLocaleString("pt-BR")} by
          <Link to={`/posts/user/${post.user_id}`}> {post.user.name}</Link>
        </div>
      </div>
    );
  }
}
