import React from "react";
import { Link } from "react-router-dom";

export default class Category extends React.Component {
  render() {
    const data = this.props.items;
    return (
      <div className="card my-4">
        <h5 className="card-header">Categories</h5>
        <div className="card-body">
          <div className="row">
            <div className="col-lg-6">
              <ul className="list-unstyled mb-0">
                {data.length > 0 &&
                  data.map((item, key) => (
                    <li key={key}>
                      <Link to={`/posts/category/${item.id}`}>{item.name}</Link>{" "}
                    </li>
                  ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
