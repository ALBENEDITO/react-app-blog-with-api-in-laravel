import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Single from "./pages/Single";
import List from "./pages/Categories/List";
import Add from "./pages/Categories/Add";
import Edit from "./pages/Categories/Edit";
import Sub from "./pages/Categories/Sub";
import FilterCategory from "./pages/FilterCategory";
import FilterUser from "./pages/FilterUser";

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) =>
        sessionStorage.getItem("meutoken") ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: "/login", state: { from: props.location } }}
          />
        )
      }
    />
  );
}

function Routes() {
  return (
    <Switch>
      <Route exact path="/login" component={Login} />
      <PrivateRoute exact path="/categories" component={List} />
      <PrivateRoute exact path="/categories/add" component={Add} />
      <PrivateRoute exact path="/categories/edit/:id" component={Edit} />
      <PrivateRoute exact path="/categories/sub" component={Sub} />
      <Route exact path="/posts/show/:id" component={Single} />
      <Route
        exact
        path="/posts/category/:category_id"
        component={FilterCategory}
      />
      <Route exact path="/posts/user/:user_id" component={FilterUser} />
      <Route exact path="/" component={Home} />

      <Redirect from="*" to="/" />
    </Switch>
  );
}

export default Routes;
