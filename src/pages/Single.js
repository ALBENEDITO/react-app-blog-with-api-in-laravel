import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import Posts from "./../components/Posts";

function Single() {
  const [posts, setPosts] = useState([]);
  const [categories, setCategories] = useState([]);
  const { id } = useParams();

  useEffect(() => {
    pegarPost();
  }, []);

  async function procurar(term = null) {
    if (term) {
      const response = await axios.get(
        `http://localhost:8000/api/posts?term=${term}`
      );
      const posts = response.data.posts;

      setPosts(posts);
    }
  }

  async function pegarPost() {
    const response = await axios.get(
      `http://localhost:8000/api/posts/show/${id}`
    );

    const posts = response.data.post;
    const categories = response.data.categories;

    setPosts([posts]);
    setCategories(categories);
  }

  return (
    <div className="App">
      <div className="container">
        <Posts
          title="View Post"
          posts={posts}
          categories={categories}
          search={procurar}
          read={true}
        />
      </div>
    </div>
  );
}

export default Single;
