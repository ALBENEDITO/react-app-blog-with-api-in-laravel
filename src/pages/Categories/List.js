import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

export default function List() {
  const [categories, setCategories] = useState([]);
  const [message, setMessage] = useState("");

  useEffect(() => {
    getCategories();
  }, []);

  async function getCategories() {
    let config = {
      headers: {
        Authorization: "Bearer " + sessionStorage.getItem("meutoken"),
      },
    };

    const response = await axios.get(
      "http://localhost:8000/api/categories",
      config
    );
    setCategories(response.data);
  }

  async function destroy(item) {
    try {
      let config = {
        headers: {
          Authorization: "Bearer " + sessionStorage.getItem("meutoken"),
        },
      };

      const response = await axios.delete(
        "http://localhost:8000/api/categories/" + item.id,
        config
      );
      setMessage(response.data.message);
      getCategories();
    } catch (error) {
      setMessage("Houve uma erro na operação, tente novamente.");
    }
  }

  return (
    <div className="App">
      <div className="container mt-5 mb-5">
        {message !== "" && <div className="alert alert-danger">{message}</div>}
        <h1>Página de listagem de categorias</h1>
        <Link to="/categories/add" className="btn btn-primary">
          Adicionar categoria
        </Link>
        <Link to="/categories/sub" className="btn btn-warning">
          Adicionar subcategoria
        </Link>
        <hr />
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Categoria</th>
              <th scope="col">Editar</th>
              <th scope="col">Deletar</th>
            </tr>
          </thead>
          <tbody>
            {categories.map((category) => (
              <tr key={category.id}>
                <th scope="row">{category.name}</th>
                <td>
                  <Link
                    to={`/categories/edit/${category.id}`}
                    className="btn btn-info"
                  >
                    Editar
                  </Link>
                </td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => destroy(category)}
                  >
                    Deletar
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
