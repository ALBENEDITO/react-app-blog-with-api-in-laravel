import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";

export default function Edit() {
  const [name, setName] = useState("");
  const [message, setMessage] = useState("");
  const history = useHistory();
  const { id } = useParams();

  useEffect(() => {
    getCategory();
  }, []);

  async function getCategory() {
    let config = {
      headers: {
        Authorization: "Bearer " + sessionStorage.getItem("meutoken"),
      },
    };

    const response = await axios.get(
      "http://localhost:8000/api/categories/" + id,
      config
    );
    setName(response.data.name);
  }

  async function onSubmit(event) {
    event.preventDefault();

    if (name !== "") {
      try {
        let config = {
          headers: {
            Authorization: "Bearer " + sessionStorage.getItem("meutoken"),
          },
        };

        await axios.put(
          "http://localhost:8000/api/categories/" + id + "/update",
          { name },
          config
        );

        history.push("/categories");
      } catch (error) {
        setMessage("Ocorreu um erro ao tentar atualizar categoria");
      }
    } else {
      setMessage("Prencha todos os campos");
    }
  }

  return (
    <div className="App">
      <div className="container mt-5 mb-5">
        <h1>Editar Categoria</h1>
        <hr />
        {message !== "" && <div className="alert alert-info">{message}</div>}
        <form onSubmit={onSubmit}>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              value={name}
              onChange={(event) => setName(event.target.value)}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Update
          </button>
        </form>
      </div>
    </div>
  );
}
