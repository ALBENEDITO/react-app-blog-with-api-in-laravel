import React, { useState } from "react";
import axios from "axios";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");

  async function onSubmit(event) {
    event.preventDefault();

    if (email !== "" && password !== "") {
      const dados = {
        grant_type: "password",
        client_id: 2,
        client_secret: "jAuGaFW1uyco2lXGXO6swcbJjLprjov64yUmVqO0",
        username: email,
        password: password,
        scope: "",
      };

      try {
        const response = await axios.post(
          "http://localhost:8000/oauth/token",
          dados
        );
        sessionStorage.setItem("meutoken", response.data.access_token);
        window.location.href = "/";
      } catch (error) {
        setMessage(error.response.data.message);
      }
    } else {
      setMessage("Prencha todos os campos");
    }
  }

  return (
    <div className="App">
      <div className="container mt-5 mb-5">
        <h1>Login</h1>
        <hr />
        {message !== "" && <div className="alert alert-danger">{message}</div>}
        <form onSubmit={onSubmit}>
          <div className="form-group">
            <label htmlFor="email">Email address</label>
            <input
              type="email"
              className="form-control"
              id="email"
              aria-describedby="emailHelp"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              className="form-control"
              id="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Enter
          </button>
        </form>
      </div>
    </div>
  );
}
