import React, { useState, useEffect } from "react";
import axios from "axios";
import Posts from "./../components/Posts";

function Home() {
  const [posts, setPosts] = useState([]);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    pegarPosts();
  }, []);

  async function procurar(term = null) {
    if (term) {
      const response = await axios.get(
        `http://localhost:8000/api/posts?term=${term}`
      );
      const posts = response.data.posts;

      setPosts(posts);
    }
  }

  async function pegarPosts() {
    const response = await axios.get(`http://localhost:8000/api/posts`);
    const posts = response.data.posts;
    const categories = response.data.categories;

    setPosts(posts);
    setCategories(categories);
  }

  return (
    <div className="App">
      <div className="container">
        <Posts
          title="Posts list"
          posts={posts}
          categories={categories}
          search={procurar}
        />
      </div>
    </div>
  );
}

export default Home;
